import { Link } from 'react-router-dom';
//import "../../../src/App.css";
import '../form/FormModule.css';

function Navbar() {
  return (
    <div class="nav-wrap">
      <div className="nav-left">
        <ul>
          <a href="/Main">
            <b>PETMILY</b>
          </a>
        </ul>
      </div>
      <div className="nav-right">
        <ul>
          <li>
            <Link to="/Prodect">
              <img
                src="https://www.karma.or.kr/images_new/common/icon_quickmenu01.png"
                alt="동물아이콘"
              />
              유실유기동물
            </Link>
          </li>
          <li>
            <Link to="/MyPage">
              <img
                src="https://www.karma.or.kr/images_new/common/icon_quickmenu02.png"
                alt="홈아이콘"
              />
              마이페이지
            </Link>
          </li>
          <li>
            <Link to="/login">
              <img
                src="https://www.karma.or.kr/images_new/common/icon_quickmenu03.png"
                alt="로그인아이콘"
              />
              로그인
            </Link>
          </li>
          <li>
            <Link to="/SignUp">
              <img
                src="https://www.karma.or.kr/images_new/common/icon_quickmenu03.png"
                alt="회원가입아이콘"
              />
              회원가입
            </Link>
          </li>
          <li>
            <Link to="/AppForm">
              <img
                src="https://www.karma.or.kr/images_new/common/icon_quickmenu03.png"
                alt="회원가입아이콘"
              />
              입양신청서
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default Navbar;
