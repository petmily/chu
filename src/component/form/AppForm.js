import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { collRegistFormAPI } from '../../api/FormAPICalls';

function AppForm() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const result = useSelector((state) => state.formReducer);

  // 입력값 저장
  const [registForm, setRegistForm] = useState({
    id: 0,
    userName: '',
    userBirth: '',
    userPhone: '',
    manageNumber: '',
    adoptReason: '',
    familyAgree: 'no',
    AdoptExperience: 'no',
    havePets: 'no',
    house: 'singleFamilyHouse',
    after: '',
    surgeryAgree: 'no',
  });

  const onChangeHandler = (e) => {
    let name = e.target.name;
    let value = e.target.value;

    // json에 저장될 데이터 타입 맞추기 위한 형변환 구문
    switch (name) {
      case 'familyAgree':
        var fobj = document.getElementsByName('familyAgree');
        fobj.value = !!fobj.value;
        break;
      case 'AdoptExperience':
        var aobj = document.getElementsByName('AdoptExperience');
        aobj.value = !!aobj.value;
        break;
      case 'havePets':
        var hobj = document.getElementsByName('havePets');
        hobj.value = !!hobj.value;
        break;
      case 'house':
        var hobj = document.getElementsByName('house');
        for (let i = 0; i < hobj.length; i++) {
          hobj[i].value = !!value;
        }
        break;
      case 'surgeryAgree':
        var sobj = document.getElementsByName('surgeryAgree');
        sobj.value = !!sobj.value;
        break;
    }

    setRegistForm({
      ...registForm,
      [name]: value,
    });
  };

  // let inputBox = document.querySelector(".inputBox");
  // let button = document.querySelector(".button");
  // button.disabled = true;
  // inputBox.addEventListener("change", stateHandle);
  //

  // 양식 제출 성공 시 실행
  useEffect(() => {
    // 등록 완료 확인 후 마이페이지로 이동 할건지???
    if (result.regist) {
      alert('신청이 완료되었습니다.');
      // navigate(`/`);
      window.location.reload();
    }
  }, [result]);

  // registForm 호출
  const onClickHdandler = () => {
    dispatch(collRegistFormAPI(registForm));
  };

  return (
    <>
      <header className="header">입양신청서</header>
      <div className="formMain">
        <hr />
        <br />
        <div>
          <strong>신청자&nbsp;&nbsp;&nbsp;&nbsp;</strong>&nbsp;&nbsp;&nbsp;
          <input
            type="text"
            className="inputBox"
            name="userName"
            value={registForm.userName}
            onChange={onChangeHandler}
            placeholder="신청자를 입력하세요"
          />
        </div>
        <br />
        <br />
        <div>
          <strong>생년월일</strong>&nbsp;&nbsp;&nbsp;&nbsp;
          <input
            type="text"
            className="inputBox"
            name="userBirth"
            value={registForm.userBirth}
            onChange={onChangeHandler}
            placeholder="생년월일을 입력하세요"
          />
        </div>
        <br />
        <br />
        <div>
          <strong>연락처&nbsp;&nbsp;&nbsp;</strong>&nbsp;&nbsp;&nbsp;&nbsp;
          <input
            type="tel"
            className="inputBox"
            name="userPhone"
            value={registForm.userPhone}
            onChange={onChangeHandler}
            placeholder="연락처를 입력하세요"
          />
        </div>
        <br />
        <br />
        <hr />
        <br />
        <div>
          <strong>일련(관리)번호</strong>&nbsp;&nbsp;&nbsp;&nbsp;
          <input
            type="number"
            className="inputBox"
            name="manageNumber"
            value={registForm.manageNumber}
            onChange={onChangeHandler}
            placeholder="일련번호를 입력하세요"
          />
        </div>
        <br />
        <br />
        <div>
          <strong>입양하려는 이유</strong>&nbsp;&nbsp;
          <input
            type="text"
            className="inputBox"
            name="adoptReason"
            value={registForm.adoptReason}
            onChange={onChangeHandler}
            placeholder="입양사유를 입력하세요"
          />
        </div>
        <br />
        <br />
        <div>
          <strong>가족동의 여부</strong>&nbsp;&nbsp;&nbsp;&nbsp;
          <label>
            <input
              type="radio"
              name="familyAgree"
              value="yes"
              onChange={onChangeHandler}
            />
            예
          </label>
          &nbsp;&nbsp;
          <label>
            <input
              type="radio"
              name="familyAgree"
              value="no"
              onChange={onChangeHandler}
            />
            아니오
          </label>
        </div>
        <br />
        <br />
        <div>
          <strong>반려동물 입양 경험</strong>&nbsp;&nbsp;&nbsp;&nbsp;
          <label>
            <input
              type="radio"
              name="AdoptExperience"
              value="yes"
              onChange={onChangeHandler}
            />
            예
          </label>
          &nbsp;&nbsp;
          <label>
            <input
              type="radio"
              name="AdoptExperience"
              value="no"
              onChange={onChangeHandler}
            />
            아니오
          </label>
        </div>
        <br />
        <br />
        <div>
          <strong>현재 반려동물을 키우고 계십니까?</strong>
          &nbsp;&nbsp;&nbsp;&nbsp;
          <label>
            <input
              type="radio"
              id="resetObj"
              name="havePets"
              value="yes"
              onChange={onChangeHandler}
            />
            예
          </label>
          &nbsp;&nbsp;
          <label>
            <input
              type="radio"
              name="havePets"
              value="no"
              onChange={onChangeHandler}
            />
            아니오
          </label>
        </div>
        <br />
        <br />
        <div>
          <strong>주거 형태</strong>&nbsp;&nbsp;&nbsp;&nbsp;
          <label>
            <input
              type="radio"
              name="house"
              value="singleFamilyHouse"
              onChange={onChangeHandler}
            />
            단독주택
          </label>
          &nbsp;&nbsp;&nbsp;
          <label>
            <input
              type="radio"
              id="resetObj"
              name="house"
              value="MultiFamilyHouse"
              onChange={onChangeHandler}
            />
            다세대주택
          </label>
          &nbsp;&nbsp;&nbsp;
          <label>
            <input
              type="radio"
              name="house"
              value="Apartment"
              onChange={onChangeHandler}
            />
            아파트
          </label>
          &nbsp;&nbsp;&nbsp;
          <label>
            <input
              type="radio"
              name="house"
              value="Others"
              onChange={onChangeHandler}
            />
            기타
          </label>
        </div>
        &nbsp;&nbsp;
        <br />
        <br />
        <div>
          <strong>
            이사, 또는 해외로 이주 시 입양한 반려동물은 어떻게 하시겠습니까?
          </strong>
          <br />
          <br />
          <input
            type="text"
            className="inputBox"
            name="after"
            value={registForm.after}
            onChange={onChangeHandler}
            placeholder="입력하세요"
          />
        </div>
        <br />
        <br />
        <hr />
        <br />
        <div>
          <strong>중성화 수술 동의</strong>&nbsp;&nbsp;&nbsp;&nbsp;
          <label>
            <input
              type="radio"
              name="surgeryAgree"
              value="yes"
              onChange={onChangeHandler}
            />
            동의
          </label>
          &nbsp;&nbsp;
          <label>
            <input
              type="radio"
              name="surgeryAgree"
              value="no"
              onChange={onChangeHandler}
            />
            동의 안함
          </label>
        </div>
        <br />
        <br />
        <div className="bt-group">
          <button class="button" onClick={onClickHdandler}>
            신청서 제출
          </button>
          &nbsp;
          {/* <button
                        type="reset"
                        class="button"
                        value="취 소"
                    ><a href="/">취 소</a></button> */}
          <input
            type="reset"
            class="button"
            value="취 소"
            onClick={() => window.location.reload(true)}
          />
        </div>
      </div>
    </>
  );
}

export default AppForm;
