import { combineReducers } from "redux";
import formReducer from "./FormModules";

const rootReducer = combineReducers({
    formReducer

});

export default rootReducer;
