import React from "react";
import { Link } from "react-router-dom";

const ProdectItem = ({ menu }) => {
  return (
    <Link to={`/menu/${menu.id}`}>
      <div className="menuItem">
        <h3>이름 : {menu.Name}</h3>
        <h3>발견장소 : {menu.place}</h3>
        <img
          src={menu.detail.image}
          style={{ maxWidth: 500 }}
          alt={menu.menuName}
        />
      </div>
    </Link>
  );
};

export default ProdectItem;
