import React from "react";
import { useEffect, useState } from "react";
import axios from "axios";
import "./Prodect.css";
import ProdectItem from "./ProdectItem";
import { useNavigate } from "react-router-dom";

const Prodect = () => {
  const [prodect, setProdect] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const Navigate = useNavigate();

  useEffect(() => {
    axios.get("http://localhost:5000/menu").then((res) => {
      setProdect(res.data);
    });
  }, []);

  const handleChange = (e) => {
    setSearchValue(e.target.value);
    Navigate(`/search?q=${e.target.value}`);
  };

  return (
    <>
      <header className="header">
        <div>
          <p className="searchText">
            당신을 기다리고 있는
            <br />
            새가족이 있습니다.
            <br />
            따뜻한 마음을 기다리고 있습니다.
          </p>

          <input
            value={searchValue}
            onChange={handleChange}
            className="searchInput"
            type="text"
            placeholder="검색어를 입력해주세요."
          />
        </div>
      </header>
      <section>
        <h1 style={{ padding: "15%", textAlign: "center" }}>
          PETMILY
          <br />
          INTRODUCE
        </h1>
        <ProdectItem prodect={prodect} />

        <div></div>
      </section>
    </>
  );
};

export default Prodect;
