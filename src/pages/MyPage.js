import React from "react";
import { useEffect, useState } from "react";
import "../myPage.css";
function MyPage({ userId }) {
  const [loading, setLoading] = useState(true);
  const [user, setUser] = useState(null);
  const [error, setError] = useState(null);
  useEffect(() => {
    window
      .fetch(`http://localhost:4000/form`)
      .then((res) => res.json())
      .then((user) => {
        setUser(user);
        setLoading(false);
      })
      .catch((error) => {
        setError(error);
        setLoading(false);
      });
  }, [userId]);
  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error!</p>;
  return (
    <>
      <header className="header">Login</header>
      <div className="mypage">
        <div className="list">
          <h1>신청 조회</h1>
        </div>
        <form className="form">
          <table>
            <tbody>
              <tr>
                <td>
                  {user.map((user) => (
                    <p>{user.userName}</p>
                  ))}
                </td>
                <td>
                  {user.map((user) => (
                    <p>{user.userBirth}</p>
                  ))}
                </td>
                <td>
                  {user.map((user) => (
                    <p>{user.userPhone}</p>
                  ))}
                </td>
                <td>
                  {user.map((user) => (
                    <p>{user.house}</p>
                  ))}
                </td>
                <td>
                  {user.map((user) => (
                    <p>{user.adoptReason}</p>
                  ))}
                </td>
              </tr>
            </tbody>
          </table>
        </form>
        <br />
        <br />
        <br />
        <br />
        <br />
        <div className="wishlist">
          <h1>찜 목록</h1>
        </div>
      </div>
    </>
  );
}
export default MyPage;
