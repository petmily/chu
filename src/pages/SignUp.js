import React from 'react';
import '../component/components/signup/SignUp.css';
function SignUp() {
  return (
    <>
      <header className="header">SignUp</header>
      <section>
        <article>
          <div className="infoBox">
            <label for="name" className="SLabel">
              아이디
              <input
                type="text"
                id="name"
                name="name"
                // ref={ usernameRef }
                placeholder="사용하실 아이디를 작성하세요."
                // value={ form.username }
                // onChange={ onChangeHandler }
              />
            </label>
            <br />
            <label for="password" className="SLabel">
              비밀번호
              <input
                type="password"
                id="password"
                name="password"
                placeholder="사용하실 비밀번호를 작성하세요."
                // value={ form.password }
                // onChange={ onChangeHandler }
              />
            </label>
            <br />
            <label for="pwcheck" className="SLabel">
              비밀번호확인
              <input
                type="password"
                id="pwcheck"
                name="pwcheck"
                // ref={ usernameRef }
                placeholder="비밀번호를 다시 한 번 작성하세요."
                // value={ form.username }
                // onChange={ onChangeHandler }
              />
            </label>
            <br />
            <label for="email" className="SLabel">
              이메일
              <input
                type="email"
                id="email"
                name="email"
                // ref={ usernameRef }
                placeholder="사용하시는 이메일을 작성하세요."
                // value={ form.username }
                // onChange={ onChangeHandler }
              />
            </label>
            <br />
            <label for="phone" className="SLabel">
              전화번호
              <input
                type="tel"
                id="phone"
                name="phone"
                // ref={ usernameRef }
                placeholder="사용하시는 전화번호를 작성하세요."
                // value={ form.username }
                // onChange={ onChangeHandler }
              />
            </label>
          </div>
          <br />
          <div className="buttonBox">
            <button
              className="completeBtn"
              // onClick={ onClickHandler }
            >
              작성완료
            </button>
            <button
            // onClick={ onClickHandler }
            >
              <a className="toLogin" href="./Login">
                취소
              </a>
            </button>
          </div>
        </article>
      </section>
    </>
  );
}
export default SignUp;
