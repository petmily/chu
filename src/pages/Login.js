import React from 'react';
import '../component/components/login/Login.css';

function Login() {
  return (
    <>
      <header className="header">Login</header>
      <section>
        <article>
          <label for="name" className="LLabel">
            아이디
            <input
              type="text"
              id="name"
              name="name"
              // ref={ usernameRef }
              placeholder="ID"
              // value={ form.username }
              // onChange={ onChangeHandler }
            />
          </label>
          <br />
          <label for="password" className="LLabel">
            비밀번호
            <input
              type="password"
              id="password"
              name="password"
              placeholder="PASSWORD"
              // value={ form.password }
              // onChange={ onChangeHandler }
            />
          </label>
          <br />
          <button
          // onClick={ onClickHandler }
          >
            로그인
          </button>
          <a className="toSignup" href="./SignUp">
            회원이 아니신가요?
          </a>
        </article>
      </section>
    </>
  );
}

export default Login;
