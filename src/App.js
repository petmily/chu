import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Layout from './layouts/Layout';
import AppForm from './component/form/AppForm';
import Login from './pages/Login';
import SingUp from './pages/SignUp';
import Main from './pages/Main';
import MyPage2 from './pages/MyPage2';
import './App.css';
import MyPage from './pages/MyPage';
import Prodect from './pages/Prodect';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Main />} />
          <Route path="login" element={<Login />} />
          <Route path="Main" element={<Main />} />
          <Route path="signup" element={<SingUp />} />
          <Route path="prodect" element={<Prodect />} />
          <Route path="mypage" element={<MyPage />} />
          <Route path="mypage2" element={<MyPage2 />} />
          <Route path="AppForm" element={<AppForm />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
