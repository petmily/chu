import { request } from "./Api";
import { getFormlist, registForm } from "../modules/FormModules";

export function callGetformListApi(){

    console.log('getFormlist api calls...');

    //미들웨어를 통한 비동기 처리
    return async(dispatch, getState) => {

        const result = await request('GET', '/form');
        console.log('getFormList result : ', result);

        // action 생성 함수에 결과 전달하며 dispatch 호출
        dispatch(getFormlist(result));
    }
}

export function collRegistFormAPI(form){

    console.log('registForm api calls...');

    return async(dispatch, getState) => {
                                                        
        const result = await request('POST', '/form/', form);

        dispatch(registForm(result));
    }

}